package main

import (
	"fmt"
	"strconv"
	"testing"
)

func TestTower_Empty(t *testing.T) {
	emptyTower := NewTower([]int{})
	if emptyTower.Empty() == false {
		t.Error("tower must be empty")
	}

	filledTower := NewTower([]int{1})
	if filledTower.Empty() {
		t.Error("tower must be filled")
	}
}

func TestTower_Hash(t *testing.T) {
	emptyTower := NewTower([]int{})
	if emptyTower.Hash() != "" {
		t.Error("tower hash must be empty string")
	}

	filledTower := NewTower([]int{3, 2, 1})
	filledHash := filledTower.Hash()
	if filledHash != "3,2,1," {
		t.Error("tower hash must be same string, but have: " + filledHash)
	}
}

func TestTower_Peek(t *testing.T) {
	filledTower := NewTower([]int{8, 7, 6})

	peek := filledTower.Peek()
	if peek != 6 {
		t.Error("Peed expect 3 but have: " + strconv.Itoa(peek))
	}

	samePeek := filledTower.Peek()
	if peek != samePeek {
		t.Error("Must be same but have: " + strconv.Itoa(samePeek))
	}
}

func TestTower_Push(t *testing.T) {
	tower := NewTower([]int{})

	tower.Push(8)

	if tower.Peek() != 8 {
		t.Error("Expect peek will be last pushedm but have:" + strconv.Itoa(tower.Peek()))
	}

	tower.Push(7)

	if tower.Peek() != 7 {
		t.Error("Expect peek will be last pushedm but have:" + strconv.Itoa(tower.Peek()))
	}
}

func TestTower_Pop(t *testing.T) {
	tower := NewTower([]int{8, 7, 6})

	top := tower.Pop()
	if top != 6 {
		t.Error("Expect pop must be 6 but have: " + strconv.Itoa(top))
	}

	expectHash := "8,7,"
	actualHash := tower.Hash()
	if expectHash != actualHash {
		t.Error("Expect after pop Tower.Hash() = " + expectHash + " but have: " + actualHash)
	}

	top = tower.Pop()
	if top != 7 {
		t.Error("Expect pop must be 7 but have: " + strconv.Itoa(top))
	}

	top = tower.Pop()
	if top != 8 {
		t.Error("Expect pop must be 8 but have: " + strconv.Itoa(top))
	}

	if tower.Empty() == false {
		t.Error("Expect after pop all tower will be empty")
	}
}

func TestState_Hash(t *testing.T) {
	state := State{
		towerMap: map[string]TowerInterface{
			"A": NewTower([]int{3, 2, 1}),
			"B": NewTower([]int{}),
			"C": NewTower([]int{}),
			"D": NewTower([]int{8, 7, 6, 5, 4}),
		},
	}

	expect := "A:3,2,1,;B:;C:;D:8,7,6,5,4,;"
	actual := state.Hash()
	if actual != expect {
		t.Error("Expect State.Hash() = " + expect + " but have: " + actual)
	}
}

func TestState_CanMoveBetween(t *testing.T) {
	state := State{
		towerMap: map[string]TowerInterface{
			"A": NewTower([]int{3, 2, 1}),
			"B": NewTower([]int{}),
			"C": NewTower([]int{}),
			"D": NewTower([]int{8, 7, 6, 5, 4}),
		},
	}

	if state.CanMoveBetween("A", "B") == false {
		t.Error("Expect that can move between A to B")
	}

	if state.CanMoveBetween("A", "D") == false {
		t.Error("Expect that can move between A to D")
	}

	if state.CanMoveBetween("B", "C") == true {
		t.Error("Expect that can not move between B to C")
	}

	if state.CanMoveBetween("D", "A") == true {
		t.Error("Expect that can not move between D to A")
	}
}

func TestState_Move(t *testing.T) {
	state := State{
		towerMap: map[string]TowerInterface{
			"A": NewTower([]int{3, 2, 1}),
			"B": NewTower([]int{}),
			"C": NewTower([]int{}),
			"D": NewTower([]int{8, 7, 6, 5, 4}),
		},
	}

	disk := state.Move("A", "B")
	if disk != 1 {
		t.Error("Expect 1 moved but got: " + strconv.Itoa(disk))
	}

	expect := "A:3,2,;B:1,;C:;D:8,7,6,5,4,;"
	actual := state.Hash()
	if actual != expect {
		t.Error("Expect State.Hash() = " + expect + " but have: " + actual)
	}

	disk = state.Move("A", "C")
	if disk != 2 {
		t.Error("Expect 1 moved but got: " + strconv.Itoa(disk))
	}

	expect = "A:3,;B:1,;C:2,;D:8,7,6,5,4,;"
	actual = state.Hash()
	if actual != expect {
		t.Error("Expect State.Hash() = " + expect + " but have: " + actual)
	}

	state.Move("A", "D")
	state.Move("C", "D")
	state.Move("B", "D")

	expect = "A:;B:;C:;D:8,7,6,5,4,3,2,1,;"
	actual = state.Hash()
	if actual != expect {
		t.Error("Expect State.Hash() = " + expect + " but have: " + actual)
	}
}

func TestFreezeState_Move(t *testing.T) {
	freezeState := NewFreezeState(map[string]TowerInterface{
		"A": NewTower([]int{3, 2, 1}),
		"B": NewTower([]int{}),
		"C": NewTower([]int{}),
		"D": NewTower([]int{}),
	})

	defer func() {
		r := recover()

		if r == nil {
			t.Error("The code must panic")
		}
	}()

	freezeState.Move("A", "B")
}

func getBeginEndInterval() (StateInterface, StateInterface) {
	emptyTower := NewTower([]int{})
	filledTower := NewTower([]int{3, 2, 1})

	begin := NewFreezeState(
		map[string]TowerInterface{
			"A": filledTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": emptyTower,
		},
	)

	end := NewFreezeState(
		map[string]TowerInterface{
			"A": emptyTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": filledTower,
		},
	)

	return begin, end
}

func assertSolveStepHistory(t *testing.T, begin StateInterface, end StateInterface, stepHistory []StepInterface) {
	current := begin.Clone()

	for index, step := range stepHistory {
		if current.CanMoveBetween(step.From(), step.To()) {
			disk := current.Move(step.From(), step.To())

			if step.Disk() != disk {
				t.Error(fmt.Sprintf("In %d step must move %d between %s and %s but moved %d", index, step.Disk(), step.From(), step.To(), disk))

				return
			}
		} else {
			t.Error(fmt.Sprintf("In %d step must move %d between %s and %s", index, step.Disk(), step.From(), step.To()))

			return
		}
	}

	if current.Hash() != end.Hash() {
		t.Error("After all steps must be end state")
	}
}

func TestBreadthFirstSearch_Solve(t *testing.T) {
	begin, end := getBeginEndInterval()
	searcher := NewBreadthFirstSearch(begin, end)

	if searcher.Solve() == false {
		t.Fatal("Expect search solve")
	}

	if len(searcher.StepHistory()) != 26 {
		t.Fatal(fmt.Sprintf("Expect step history count 26, but receive: %d", len(searcher.StepHistory())))
	}

	assertSolveStepHistory(t, begin, end, searcher.StepHistory())
	StoreStepHistory("breadthFirstSearchOutput.txt", searcher.StepHistory())
}

func TestBestFirstSearch_Solve(t *testing.T) {
	begin, end := getBeginEndInterval()
	searcher := NewBestFirstSearch(begin, end)

	if searcher.Solve() == false {
		t.Fatal("Expect search solve")
	}

	if len(searcher.StepHistory()) != 6 {
		t.Fatal(fmt.Sprintf("Expect step history count 6, but receive: %d", len(searcher.StepHistory())))
	}

	assertSolveStepHistory(t, begin, end, searcher.StepHistory())
	StoreStepHistory("bestFirstSearchOutput.txt", searcher.StepHistory())
}

func TestDeepFirstSearch_Solve(t *testing.T) {
	begin, end := getBeginEndInterval()
	searcher := NewDeepFirstSearch(begin, end)

	if searcher.Solve() == false {
		t.Fatal("Expect search solve")
	}

	assertSolveStepHistory(t, begin, end, searcher.StepHistory())
	StoreStepHistory("deepFirstSearchOutput.txt", searcher.StepHistory())
}

func BenchmarkState_Hash(b *testing.B) {
	state := State{
		towerMap: map[string]TowerInterface{
			"A": NewTower([]int{3, 2, 1}),
			"B": NewTower([]int{}),
			"C": NewTower([]int{}),
			"D": NewTower([]int{8, 7, 6, 5, 4}),
		},
	}

	for i := 0; i < b.N; i++ {
		state.Hash()
	}
}

func BenchmarkTower_Hash(b *testing.B) {
	tower := NewTower([]int{8, 7, 6, 5, 4})

	for i := 0; i < b.N; i++ {
		tower.Hash()
	}
}

func BenchmarkBreadthFirstSearch_Solve(b *testing.B) {
	emptyTower := NewTower([]int{})
	filledTower := NewTower([]int{3, 2, 1})

	begin := NewFreezeState(
		map[string]TowerInterface{
			"A": filledTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": emptyTower,
		},
	)

	end := NewFreezeState(
		map[string]TowerInterface{
			"A": emptyTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": filledTower,
		},
	)

	var searcher SearchInterface
	for i := 0; i < b.N; i++ {
		searcher = NewBreadthFirstSearch(begin, end)
		searcher.Solve()
	}

	StoreStepHistory("breadthFirstSearchOutput.txt", searcher.StepHistory())
}

func BenchmarkBestFirstSearch_Solve(b *testing.B) {
	emptyTower := NewTower([]int{})
	filledTower := NewTower([]int{3, 2, 1})

	begin := NewFreezeState(
		map[string]TowerInterface{
			"A": filledTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": emptyTower,
		},
	)

	end := NewFreezeState(
		map[string]TowerInterface{
			"A": emptyTower,
			"B": emptyTower,
			"C": emptyTower,
			"D": filledTower,
		},
	)

	var searcher SearchInterface
	for i := 0; i < b.N; i++ {
		searcher = NewBestFirstSearch(begin, end)
		searcher.Solve()
	}

	StoreStepHistory("bestFirstSearchOutput.txt", searcher.StepHistory())
}
