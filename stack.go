package main

type Stack struct {
	values []int
	size   int
}

func (s *Stack) Push(value int) {
	if s.size < len(s.values) {
		s.values[s.size] = value
	} else {
		s.values = append(s.values, value)
	}
	s.size++
}

func (s *Stack) Peek() int {
	return s.values[s.size-1]
}

func (s *Stack) Pop() int {
	s.size--
	return s.values[s.size]
}

func (s *Stack) Clone() *Stack {
	values := make([]int, s.size)
	copy(values, s.values[:s.size])
	return &Stack{values, s.size}
}

func (s *Stack) Len() int {
	return s.size
}

func (s *Stack) Iterator() []int {
	return s.values[:s.size]
}

func NewStackBySlice(values []int) *Stack {
	return &Stack{
		values: values,
		size:   len(values),
	}
}
